# Wiki

Comme Gitlab ne permet pas de créer un wiki lié à un groupe de projet, j'ai décidé de créer un dépôt "Wiki" qui contient la documentation de tous les autres dépôt. Ce wiki est disponible [ici](https://gitlab.com/projetweb3/wiki/-/wikis/home).